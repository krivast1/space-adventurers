
// #define NDEBUG // comment to enable asserts

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#endif // _WIN32

#include <iostream>
#include <iomanip>
#include <thread>
#include <string>
#include <cstring>

#include "C_Game.hpp"

#define GAME_WIDTH 120
#define GAME_HEIGHT 25

#define GAME_MIN_WIDTH 50
#define GAME_MAX_WIDTH 200

#define GAME_MIN_HEIGHT 10
#define GAME_MAX_HEIGHT 50

struct S_GameArgs
{
    uint32_t gameWidth = GAME_WIDTH;
    uint32_t gameHeight = GAME_HEIGHT;
    bool startGame = true;
};

int str2int(int* res, const char* str)
{
    try
    {
        *res = std::stoi(str);
    }
    catch (const std::invalid_argument& e)
    {
        return -1;
    }
    catch (const std::out_of_range& e)
    {
        return -2;
    }

    return 0;
}

void invalidArgumentMessage(char* arg)
{
    std::cerr << "[Warning] Invalid argument: " << arg << ". See -help" << std::endl;
}

S_GameArgs processInput(int argc, char* argv[])
{
    S_GameArgs args = { GAME_WIDTH, GAME_HEIGHT, true };

    for (int i = 1; i < argc; i += 2)
    {
        if (std::strlen(argv[i]) <= 1)
        {
            invalidArgumentMessage(argv[i]);
            continue;
        }

        int tmp;
        switch (argv[i][1]) {
            case 'w':
                if ((std::strlen(argv[i]) <= 2 || !std::strcmp(argv[i], "-width")) && (i + 1) < argc &&
                    str2int(&tmp, argv[i + 1]) >= 0 && tmp >= GAME_MIN_WIDTH && tmp <= GAME_MAX_WIDTH)
                {
                    args.gameWidth = tmp;
                }
                else
                {
                    invalidArgumentMessage(argv[i]);
                }
                break;

            case 'h':
                if ((std::strlen(argv[i]) <= 2 || !std::strcmp(argv[i], "-height")) && (i + 1) < argc &&
                    str2int(&tmp, argv[i + 1]) >= 0 && tmp >= GAME_MIN_HEIGHT && tmp <= GAME_MAX_HEIGHT)
                {
                    args.gameHeight = tmp;
                }
                else if (!std::strcmp(argv[i], "-help"))
                {
                    std::cout << std::left;
                    std::cout << std::setw(20) << " -w -width [num] " << "Sets width of the game."
                        << " Must be in range <"<< GAME_MIN_WIDTH << ", " << GAME_MAX_WIDTH << ">" << std::endl;

                    std::cout << std::setw(20) << " -h -height [num] " << "Sets height of the game."
                        << " Must be in range <" << GAME_MIN_HEIGHT << ", " << GAME_MAX_HEIGHT << ">" << std::endl;

                    std::cout << std::setw(20) << " -c -controls " << "Shows controls" << std::endl;

                    args.startGame = false;
                }
                else
                {
                    invalidArgumentMessage(argv[i]);
                }
                break;

            case 'c':
                if (std::strlen(argv[i]) <= 2 || !std::strcmp(argv[i], "-controls"))
                {
                    std::cout << std::left;
                    std::cout << "Controls:" << std::endl;
                    std::cout << std::setw(5) << " w " << " Move up" << std::endl;
                    std::cout << std::setw(5) << " a " << " Move left" << std::endl;
                    std::cout << std::setw(5) << " s " << " Move down" << std::endl;
                    std::cout << std::setw(5) << " d " << " Move right" << std::endl;
                    std::cout << std::setw(5) << " space " << " Fire/Shoot" << std::endl;
                    std::cout << std::setw(5) << " r " << " Restart game (only if dead)" << std::endl;
                    std::cout << std::setw(5) << " q " << " Quit game" << std::endl;
                    args.startGame = false;
                }
                else
                {
                    invalidArgumentMessage(argv[i]);
                }
                break;

            default:
                invalidArgumentMessage(argv[i]);
                break;
        }
    }

    return args;
}

void setRaw(bool set)
{
#ifdef _WIN32
    if (set)
    {
        SetConsoleMode(GetStdHandle(STD_INPUT_HANDLE), ENABLE_WINDOW_INPUT);
    }
    else
    {
        SetConsoleMode(GetStdHandle(STD_INPUT_HANDLE), ~ENABLE_WINDOW_INPUT);
    }
#else
    if (set)
    {
        system("stty raw");  // enable raw
    }
    else
    {
        system("stty -raw"); // disable raw
    }
#endif // _WIN32
}

int main(int argc, char* argv[]) {
    S_GameArgs args = processInput(argc, argv);

    if (args.startGame)
    {
        auto logicThread = [](C_Game& game)
        {
            while (!game.GetQuit())
            {
                game.Tick();
            }
        };

        auto outputThread = [](C_Game& game)
        {
            while (!game.GetQuit())
            {
                game.Render();
            }
        };

        auto inputThread = [](C_Game& game)
        {
            while (!game.GetQuit())
            {
                game.UpdateInputs();
            }
        };

        setRaw(true);

        C_Game game(args.gameWidth, args.gameHeight);
        std::thread t1(logicThread, std::ref(game));
        std::thread t2(outputThread, std::ref(game));
        std::thread t3(inputThread, std::ref(game));

        t1.join();
        t2.join();
        t3.join();

        setRaw(false);
    }

    return 0;
}
