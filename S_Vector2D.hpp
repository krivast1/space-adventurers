#pragma once

#include <vector>
#include <cmath>

#include "common.hpp"

template<typename T> struct S_Bounds;

template<typename T>
struct S_Vector2D
{
    T x;
    T y;

    template<typename U>
    S_Vector2D<U> CastTo() const { return { static_cast<U>(x), static_cast<U>(y) }; }

    S_Vector2D& operator+=(S_Vector2D const& other);
    S_Vector2D& operator-=(S_Vector2D const& other);
    S_Vector2D& operator*=(T scalar);
    S_Vector2D& operator/=(T scalar);
    
    S_Vector2D operator+(S_Vector2D const& other) const;
    S_Vector2D operator-(S_Vector2D const& other) const;
    S_Vector2D operator*(T scalar) const;
    S_Vector2D operator/(T scalar) const;

    bool operator==(S_Vector2D const& other) const;
    bool operator!=(S_Vector2D const& other) const;

    float Length() const;

    S_Vector2D& Normalize();
    S_Vector2D Normalized() const;

    void ToBounds(S_Bounds<T> const& bounds);

    static const S_Vector2D ZERO;
    static const S_Vector2D UP;
    static const S_Vector2D RIGHT;
    static const S_Vector2D DOWN;
    static const S_Vector2D LEFT;  
};

template<typename T>
struct S_Bounds
{
    S_Vector2D<T> min = { 0, 0 };
    S_Vector2D<T> max = { 0, 0 };

    T GetWidth() const { return abs(max.x - min.x); }
    T GetHeight() const { return abs(max.y - min.y); }
};

using T_CoordElement = int32_t;
using T_Coord = S_Vector2D<T_CoordElement>;
using T_Coords = std::vector<T_Coord>;
using S_CoordBounds = S_Bounds<T_CoordElement>;

template<typename T> const S_Vector2D<T> S_Vector2D<T>::ZERO  = { 0, 0};
template<typename T> const S_Vector2D<T> S_Vector2D<T>::UP    = { 0,-1};
template<typename T> const S_Vector2D<T> S_Vector2D<T>::RIGHT = { 1, 0};
template<typename T> const S_Vector2D<T> S_Vector2D<T>::DOWN  = { 0, 1};
template<typename T> const S_Vector2D<T> S_Vector2D<T>::LEFT  = {-1, 0};


template<typename T>
S_Vector2D<T>& S_Vector2D<T>::operator+=(S_Vector2D<T> const& other)
{
    x += other.x;
    y += other.y;
    return *this;
}

template<typename T>
S_Vector2D<T>& S_Vector2D<T>::operator-=(S_Vector2D const& other)
{
    x -= other.x;
    y -= other.y;
    return *this;
}

template<typename T>
S_Vector2D<T>& S_Vector2D<T>::operator*=(T scalar)
{
    x *= scalar;
    y *= scalar;
    return *this;
}

template<typename T>
S_Vector2D<T>& S_Vector2D<T>::operator/=(T scalar)
{
    x /= scalar;
    y /= scalar;
    return *this;
}

template<typename T>
S_Vector2D<T> S_Vector2D<T>::operator+(S_Vector2D<T> const& other) const
{
    return S_Vector2D(*this) += other;
}

template<typename T>
S_Vector2D<T> S_Vector2D<T>::operator-(S_Vector2D const& other) const
{
    return S_Vector2D(*this) -= other;
}

template<typename T>
S_Vector2D<T> S_Vector2D<T>::operator*(T scalar) const
{
    return S_Vector2D(*this) *= scalar;
}

template<typename T>
S_Vector2D<T> S_Vector2D<T>::operator/(T scalar) const
{
    return S_Vector2D(*this) /= scalar;
}

template<typename T>
bool S_Vector2D<T>::operator==(S_Vector2D const& other) const
{
    return x == other.x && y == other.y;
}

template<typename T>
bool S_Vector2D<T>::operator!=(S_Vector2D const& other) const
{
    return !(*this == other);
}

template<typename T>
float S_Vector2D<T>::Length() const
{
    return std::sqrt(x * x + y * y);
}

template<typename T>
S_Vector2D<T>& S_Vector2D<T>::Normalize()
{
    float length = Length();
    if (length > 0)
    {
        *this /= length;
    }
    return *this;
}

template<typename T>
S_Vector2D<T> S_Vector2D<T>::Normalized() const
{
   return S_Vector2D(*this).Normalize();
}

template<typename T>
void S_Vector2D<T>::ToBounds(S_Bounds<T> const& bounds)
{
    x = std::max(bounds.min.x, std::min(bounds.max.x, x));
    y = std::max(bounds.min.y, std::min(bounds.max.y, y));
}
