#pragma once

#include <vector>

#include "C_Collider.hpp"

class C_PixelCollider : public C_Collider
{
public:
	C_PixelCollider() = delete;
	C_PixelCollider(T_GameObjectPtr const& ptr) : C_Collider(ptr) {}
	
	DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_PixelCollider)

	virtual E_ColliderType GetType() const override { return E_ColliderType::PIXEL_COLLIDER; }

	virtual void Update() override;

private:
	virtual bool DoesCollideWith(C_Collider const& other) const override;
};
