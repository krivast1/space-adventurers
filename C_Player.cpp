#include "C_Player.hpp"
#include "C_PixelCollider.hpp"

std::shared_ptr<C_Shot> C_Player::Shoot() const
{
    S_Vector2D<float> direction = GetOrientation().AsVector();
    S_Vector2D<float> position = GetPosition() + direction;

    std::shared_ptr<C_PlayerShot> shot = std::make_shared<C_PlayerShot>(position, direction, m_ShotSpeed);
    shot->SetCollider(std::make_unique<C_PixelCollider>(shot));

    return shot;
}
