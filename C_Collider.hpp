#pragma once

#include <vector>
#include <memory>
#include <functional>

#include "S_Vector2D.hpp"

enum class E_ColliderType
{
	EMPTY_COLLIDER,
	PIXEL_COLLIDER
};

enum class E_GameObjectType : uint32_t;
struct S_CollisionInfo
{
	E_GameObjectType otherObjectType;
	bool didCollide = false;
};

class C_GameObject;
class C_Collider
{
protected:
	using T_GameObjectPtr = std::shared_ptr<C_GameObject>;

private:
	using T_Pred = std::function<bool(C_Collider const&)>;

public:
	C_Collider() = default;
	C_Collider(T_GameObjectPtr const& ptr) : m_GameObjectPtr(ptr) {}

	DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_Collider)

	virtual E_ColliderType GetType() const = 0;

	virtual void Update();

	void ClearCollisionInfo() { m_CollisionInfo = {}; }
	bool CheckCollision(C_Collider& other, T_Pred setInfoPredicate);

	bool DidCollide() const { return m_CollisionInfo.didCollide; }
	S_CollisionInfo const& GetCollisionInfo() const { return m_CollisionInfo; }
	T_GameObjectPtr GetGameObject() const { return m_GameObjectPtr.lock(); }
	T_Coords const& GetGlobalCoords() const { return m_GlobalCoords; }
	S_CoordBounds const& GetGlobalBounds() const { return m_Bounds; }

private:
	virtual bool DoesCollideWith(C_Collider const& other) const = 0;

protected:
	T_Coords m_GlobalCoords = {};

private:
	std::weak_ptr<C_GameObject> m_GameObjectPtr;
	S_CollisionInfo m_CollisionInfo = {};
	S_CoordBounds m_Bounds = {};
};
