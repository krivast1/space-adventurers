#pragma once

#include "C_Collider.hpp"

class C_EmptyCollider : public C_Collider
{
public:
	C_EmptyCollider() = default;

	DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_EmptyCollider)

	virtual void Update() override {}

	virtual E_ColliderType GetType() const override { return E_ColliderType::EMPTY_COLLIDER; }

private:
	virtual bool DoesCollideWith(C_Collider const& /*other*/) const override { return false; }
};
