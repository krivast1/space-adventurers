# Space-Adventurers

A small ascii art console game where you cruise your way through the space. You must avoid asteroids floating around and missiles targeting you.

This project is a part of the semestral project for the subject Programming in C/C++. The original assignment was to create a real-time game in which you fly in a grid space and you shoot down asteroids and missiles. The asteroids have a steady speed and direction, but the missiles follow you. You must survive as long as possible and you get points for the time you survive and the asteroids you shoot down.

# Implementation

The whole game is written from scratch in C++ and compiled by cmake. The game is compatible with Linux and Windows. You can scale the width and height by -w and -h arguments respectively.

# Scoring

The target of the game is to achieve the highest score possible.
For each second you survive you get 1 point and for each asteroid you destroy you get 10 points.

# Controls

- movement: w,a,s,d
- shoot: space
- restart: r
- quit: q

# Visuals

You - V, >, <, ^\
Asteroid - @\
Missile - \*

# Install

Download the project and compile with cmake "cmake CMakeLists.txt". Then run ./SpaceAdventurers.

You may need to enlarge your console window to see the whole game properly (or adjust the width and height of the game).

Use -help for more info.
