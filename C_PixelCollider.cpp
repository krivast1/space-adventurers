
#include <algorithm>
#include <type_traits>
#include <iostream>

#include "C_PixelCollider.hpp"

void C_PixelCollider::Update()
{
	C_Collider::Update();

	std::sort(m_GlobalCoords.begin(), m_GlobalCoords.end(),
		[](auto const& lhs, auto const& rhs)
		{
			return lhs.y == rhs.y ? lhs.x < rhs.x : lhs.y < rhs.y;
		});
}

bool C_PixelCollider::DoesCollideWith(C_Collider const& other) const
{
	if (other.GetType() == E_ColliderType::EMPTY_COLLIDER)
	{
		return false;
	}

	if (GetType() != other.GetType())
	{
		std::cerr << "Error: Collision detection of C_PixelCollider and "
			<< typeid(other).name() << " not implemented!" << std::endl;
		return false;
	}

	T_Coords otherCoords = other.GetGlobalCoords();
	auto thisIt = m_GlobalCoords.cbegin();
	auto otherIt = otherCoords.cbegin();

	while (thisIt != m_GlobalCoords.cend() && otherIt != otherCoords.cend())
	{
		if (thisIt->x == otherIt->x && thisIt->y == otherIt->y)
		{
			return true;
		}

		bool thisIsLower = thisIt->y == otherIt->y ? thisIt->x < otherIt->x : thisIt->y < otherIt->y;
		if (thisIsLower)
		{
			++thisIt;
		}
		else
		{
			++otherIt;
		}
	}

	return false;
}
