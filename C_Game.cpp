#include <iostream>
#include <algorithm>

#include "C_Game.hpp"
#include "C_Asteroid.hpp"
#include "C_Bomb.hpp"
#include "C_PixelCollider.hpp"

C_Game::C_Game(uint32_t width, uint32_t height)
    : m_Window(std::cout, width, height)
    , m_AsteriodCountLimit(width * height / ASTEROID_LIMIT_FACTOR)
    , m_BombCountLimit(width * height / BOMB_LIMIT_FACTOR)
{
    Restart();
}

void C_Game::Tick()
{
    ExecuteAfterSleep(m_LastTickExec, T_MS(GAME_LOGIC_PERIOD));
    ++m_TickNumber;

    ProcessInputs();
    SpawnRandomGameObjects();
    UpdateObjects();
    UpdateScore();
}

void C_Game::UpdateInputs()
{
    char c = static_cast<char>(getchar());

    if (c == 'q')
    {
        m_Quit = true;
        return;
    }

    if (c == 'r' && m_GameOver)
    {
        Restart();
    }
    else if (c == ' ' || c == 'w' || c == 'a' || c == 's' || c == 'd')
    {
        std::lock_guard<std::mutex> lock(m_InputMutex);

        if (c == ' ')
        {
            m_Inputs.fire = true;
        }
        else
        {
            switch (c)
            {
                case 'w':
                    m_Inputs.orientation = E_Orientation::UP;
                    break;
                case 'a':
                    m_Inputs.orientation = E_Orientation::LEFT;
                    break;
                case 's':
                    m_Inputs.orientation = E_Orientation::DOWN;
                    break;
                case 'd':
                    m_Inputs.orientation = E_Orientation::RIGHT;
                    break;
            }
        }
    }
}

void C_Game::Render()
{
    ExecuteAfterSleep(m_LastRenderExec, T_MS(GAME_RENDER_PERIOD));
    std::lock_guard<std::recursive_mutex> lock(m_GameObjectsMutex);

    m_Window.GetCanvas().Clear();
    std::for_each(m_GameObjects.cbegin(), m_GameObjects.cend(),
        [this](T_GameObjectPtr const& obj) { obj->Draw(m_Window.GetCanvas()); });

    m_Window.Render();
}

std::shared_ptr<C_Asteroid> C_Game::SpawnAsteroid(
    S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
{
    std::shared_ptr<C_Asteroid> asteroid = std::make_shared<C_Asteroid>(position, direction, speed);
    asteroid->SetCollider(std::make_unique<C_PixelCollider>(asteroid));
    SpawnGameObject(asteroid);

    return asteroid;
}

std::shared_ptr<C_Bomb> C_Game::SpawnBomb(
    S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
{
    std::shared_ptr<C_Bomb> bomb = std::make_shared<C_Bomb>(position, direction, speed, m_Player);
    bomb->SetCollider(std::make_unique<C_PixelCollider>(bomb));
    SpawnGameObject(bomb);

    return bomb;
}

void C_Game::Restart()
{
    std::lock_guard<std::recursive_mutex> lock(m_GameObjectsMutex);

    m_GameObjects.clear();

    m_LastTickExec = {};
    m_LastRenderExec = {};

    m_TickNumber = 0;
    m_ScoreIncCount = 0;

    m_GameObjectCounts = {};

    m_Window.ResetScore();
    m_Window.ClearText();

    m_GameOver = false;

    SpawnPlayer();
}

void C_Game::SpawnPlayer()
{
    C_Canvas const& canvas = m_Window.GetCanvas();

    m_Player = std::make_shared<C_Player>(
        S_Vector2D<float>{static_cast<float>(canvas.GetWidth()) / 2, static_cast<float>(canvas.GetHeight()) / 2},
        S_Vector2D<float>::ZERO,
        PLAYER_SPEED
    );
    m_Player->SetCollider(std::make_unique<C_PixelCollider>(m_Player));

    S_CoordBounds playerBounds = m_Player->GetCollider().GetGlobalBounds();
    float playerHalfWidth = static_cast<float>((playerBounds.GetWidth() + 1) / 2);
    float playerHalfHeight = static_cast<float>((playerBounds.GetHeight() + 1) / 2);

    S_Vector2D<float> boundsMin = { playerHalfWidth, playerHalfHeight };
    S_Vector2D<float> boundsMax = {
        static_cast<float>(canvas.GetWidth() - 1) - playerHalfWidth,
        static_cast<float>(canvas.GetHeight() - 1) - playerHalfHeight
    };

    m_Player->SetPositionBounds({ boundsMin, boundsMax });

    SpawnGameObject(m_Player);
}

void C_Game::SpawnGameObject(T_GameObjectPtr object)
{
    assert(object != nullptr && "Can not spawn nullptr!");
    assert(object->GetCollider().GetGameObject() != nullptr && "Spawning game object without collider is not allowed!");

    std::lock_guard<std::recursive_mutex> lock(m_GameObjectsMutex);

    m_GameObjectCounts[GetEnumUnderlyingType(object->GetType())] += 1;
    m_GameObjects.push_back(std::move(object));
}

void C_Game::SpawnRandomGameObjects()
{
    std::lock_guard<std::recursive_mutex> lock(m_GameObjectsMutex);

    if (GetGameObjectCount(E_GameObjectType::ASTEROID) < m_AsteriodCountLimit && DidPassRandom())
    {
        SetRandomTransformOutside(SpawnAsteroid({}, {}, 0), ASTEROID_MIN_SPEED, ASTEROID_MAX_SPEED);
    }

    if (GetGameObjectCount(E_GameObjectType::BOMB) < m_BombCountLimit && DidPassRandom())
    {
        SetRandomTransformOutside(SpawnBomb({}, {}, 0), BOMB_MIN_SPEED, BOMB_MAX_SPEED);
    }
}

void C_Game::ProcessInputs()
{
    std::lock<std::mutex, std::recursive_mutex>(m_InputMutex, m_GameObjectsMutex);
    std::lock_guard<std::mutex> lock1(m_InputMutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lock2(m_GameObjectsMutex, std::adopt_lock);

    if (m_Player != nullptr)
    {
        if (m_Inputs.fire)
        {
            SpawnGameObject(m_Player->Shoot());
        }

        m_Player->SetOrientation(m_Inputs.orientation);
    }

    m_Inputs = {};
}

void C_Game::UpdateObjects()
{
    std::lock_guard<std::recursive_mutex> lock(m_GameObjectsMutex);

    std::for_each(m_GameObjects.begin(), m_GameObjects.end(), [](T_GameObjectPtr const& obj) { obj->Update(); });

    CheckCollisions();
    UpdatePlayer();
    HandleCollisions();
}

void C_Game::CheckCollisions()
{
    // Update collision info only if the collider did not collide yet
    auto predicate = [](C_Collider const& collider) { return !collider.DidCollide(); };

    for (auto it = m_GameObjects.begin(); it != m_GameObjects.end(); ++it)
    {
        C_Collider& collider = (*it)->GetCollider();
        for (auto it2 = std::next(it, 1); it2 != m_GameObjects.end(); ++it2)
        {
            collider.CheckCollision((*it2)->GetCollider(), predicate);
        }
    }
}

void C_Game::HandleCollisions()
{
    std::for_each(m_GameObjects.begin(), m_GameObjects.end(), [this](T_GameObjectPtr const& gameObjectPtr)
        {
            C_Collider const& collider = gameObjectPtr->GetCollider();
            if (collider.DidCollide())
            {
                C_Shot const* shot = dynamic_cast<C_Shot const*>(gameObjectPtr.get());
                if (shot != nullptr && shot->IsFromPlayer())
                {
                    m_Window.AddScore(SCORE_ASTEROID);
                }

                gameObjectPtr->HandleCollision(collider.GetCollisionInfo());
            }
        });
    
    // Checks whether an object is out of game bounds and is not even moving towards them
    auto willBeOutOfBounds = [width = static_cast<T_CoordElement>(m_Window.GetCanvas().GetWidth()),
                              height = static_cast<T_CoordElement>(m_Window.GetCanvas().GetHeight())]
        (C_GameObject const& gameObject)
        {
            S_CoordBounds const& bounds = gameObject.GetCollider().GetGlobalBounds();
            S_Vector2D<float> dir = gameObject.GetDirection() * gameObject.GetSpeed();

            return (bounds.max.x < 0 && dir.x <= 0) || (bounds.min.x >= width && dir.x >= 0) ||
                   (bounds.max.y < 0 && dir.y <= 0) || (bounds.min.y >= height && dir.y >= 0);
        };

    // Remove objects which collided or won't ever be in game bounds
    auto const it = std::remove_if(m_GameObjects.begin(), m_GameObjects.end(),
        [this, willBeOutOfBounds] (T_GameObjectPtr const& gameObjectPtr)
        { 
            bool toBeRemoved = gameObjectPtr->GetCollider().DidCollide() || willBeOutOfBounds(*gameObjectPtr);
            if (toBeRemoved)
            {
                m_GameObjectCounts[GetEnumUnderlyingType(gameObjectPtr->GetType())] -= 1;
            }
            return toBeRemoved;
        });
    m_GameObjects.erase(it, m_GameObjects.end());
}

void C_Game::UpdatePlayer()
{
    if (m_Player != nullptr && m_Player->GetCollider().DidCollide())
    {
        m_GameOver = true;
        m_Window.SetText("Game Over! Press 'r' to restart");
        m_Player = nullptr;
    }
}

void C_Game::UpdateScore()
{
    assert(SCORE_INC_PERIOD > 0 && "Score increment period must be positive");

    if (m_GameOver)
    {
        return;
    }

    uint64_t timePassed = (m_TickNumber) * GAME_LOGIC_PERIOD;
    uint64_t incToPerformSinceStart = timePassed / SCORE_INC_PERIOD;
    if (m_ScoreIncCount < incToPerformSinceStart)
    {
        m_Window.AddScore(SCORE_INC);
        ++m_ScoreIncCount;
    }
}

void C_Game::SetRandomTransformOutside(T_GameObjectPtr const& gameObjectPtr, float minSpeed, float maxSpeed) const
{
    assert(gameObjectPtr != nullptr);

    S_CoordBounds const& bounds = gameObjectPtr->GetCollider().GetGlobalBounds();

    gameObjectPtr->SetPosition(GenerateRandomPositionOutside(bounds.GetWidth() / 2, bounds.GetHeight() / 2));
    gameObjectPtr->SetDirection(GenerateRandomDirectionInside(gameObjectPtr->GetPosition()));
    gameObjectPtr->SetSpeed(GetRandomFloat(minSpeed, maxSpeed));
}

S_Vector2D<float> C_Game::GenerateRandomPositionOutside(T_CoordElement xOffset, T_CoordElement yOffset) const
{
    C_Canvas const& canvas = m_Window.GetCanvas();

    if (DidPassRandom())
    {
        float x = static_cast<float>(rand() % canvas.GetWidth());
        float y = static_cast<float>(DidPassRandom() ? 0 - yOffset : canvas.GetHeight() + yOffset);
        return { x, y };
    }
    else
    {
        float x = static_cast<float>(DidPassRandom() ? 0 - xOffset : canvas.GetWidth() + xOffset);
        float y = static_cast<float>(rand() % canvas.GetHeight());
        return { x, y };
    }
}

S_Vector2D<float> C_Game::GenerateRandomDirectionInside(S_Vector2D<float> const& position) const
{
    C_Canvas const& canvas = m_Window.GetCanvas();

    S_Vector2D<float> pointInside = {
        static_cast<float>(rand() % canvas.GetWidth()),
        static_cast<float>(rand() % canvas.GetHeight())
    };

    return pointInside - position;
}

void C_Game::ExecuteAfterSleep(T_TimePoint& lastExecutionTime, T_MS const& executionPeriod)
{
    T_TimePoint currentTime = std::chrono::high_resolution_clock::now();
    T_MS timePassed = std::chrono::duration_cast<T_MS>(currentTime - lastExecutionTime);
    T_MS timeToSleep = std::max(T_MS(0), executionPeriod - timePassed);
    
    std::this_thread::sleep_for(timeToSleep);

    lastExecutionTime = currentTime + timeToSleep;
}
