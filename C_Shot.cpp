#include "C_Shot.hpp"

void C_Shot::Draw(C_Canvas& canvas) const
{
    canvas.SetPixel(LocalToGlobal(m_LocalCoords[0]), 'o');
}

T_Coords const& C_Shot::GetLocalCoords() const
{
    return m_LocalCoords;
}

void C_Shot::UpdateInternal()
{
    Move();
}

void C_Shot::HandleCollisionInternal(S_CollisionInfo const& /*collisionInfo*/)
{
    
}
