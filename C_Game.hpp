#pragma once

#include <vector>
#include <array>
#include <memory>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "C_Window.hpp"
#include "S_Inputs.hpp"
#include "C_Player.hpp"
#include "C_Asteroid.hpp"
#include "C_Bomb.hpp"

#define GAME_LOGIC_PERIOD 100 // in ms

#define SCORE_INC_PERIOD 1000 // in ms
#define SCORE_INC 1
#define SCORE_ASTEROID 10

#define GAME_FPS_CAP 60
#define GAME_RENDER_PERIOD (1000 / GAME_FPS_CAP) // in ms

#define PLAYER_SPEED 1.0f

#define ASTEROID_LIMIT_FACTOR 150
#define BOMB_LIMIT_FACTOR 600

class C_Game
{
    using T_TimePoint = std::chrono::high_resolution_clock::time_point;
    using T_MS = std::chrono::milliseconds;
    using T_GameObjectPtr = std::shared_ptr<C_GameObject>;
    using T_GameObjectTypeArray = std::array<uint32_t, GetEnumUnderlyingType(E_GameObjectType::COUNT)>;

public:
    C_Game(uint32_t width, uint32_t height);

    void Tick();
    void UpdateInputs();
    void Render();

    std::shared_ptr<C_Asteroid> SpawnAsteroid(
        S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed);
    
    std::shared_ptr<C_Bomb> SpawnBomb(
        S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed);

    bool GetQuit() const { return m_Quit; }

private:
    void Restart();
    void SpawnPlayer();
    void SpawnGameObject(T_GameObjectPtr object);
    void SpawnRandomGameObjects();
    void ProcessInputs();
    void UpdateObjects();
    void CheckCollisions();
    void HandleCollisions();
    void UpdatePlayer();
    void UpdateScore();

    void SetRandomTransformOutside(T_GameObjectPtr const& gameObjectPtr, float minSpeed, float maxSpeed) const;
    S_Vector2D<float> GenerateRandomPositionOutside(T_CoordElement xOffset, T_CoordElement yOffset) const;
    S_Vector2D<float> GenerateRandomDirectionInside(S_Vector2D<float> const& position) const;

    uint32_t GetGameObjectCount(E_GameObjectType type) const { return m_GameObjectCounts[GetEnumUnderlyingType(type)]; }

    static void ExecuteAfterSleep(T_TimePoint& lastExecutionTime, T_MS const& executionPeriod);

    C_Window m_Window;
    S_Inputs m_Inputs = {};

    std::shared_ptr<C_Player> m_Player; 
    std::vector<T_GameObjectPtr> m_GameObjects = {};

    T_TimePoint m_LastTickExec = {};
    T_TimePoint m_LastRenderExec = {};
    
    uint64_t m_TickNumber = 0;
    uint64_t m_ScoreIncCount = 0;

    std::mutex m_InputMutex;
    std::recursive_mutex m_GameObjectsMutex;

    uint32_t const m_AsteriodCountLimit;
    uint32_t const m_BombCountLimit;

    T_GameObjectTypeArray m_GameObjectCounts = {};

    bool m_GameOver = false;
    bool m_Quit = false;
};
