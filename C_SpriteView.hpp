#pragma once

#include <vector>

#include "S_Vector2D.hpp"

class C_SpriteView
{
public:
	using T_Sprite = std::vector<std::vector<bool>>;

	C_SpriteView(T_Sprite const& shape, char symbol);

	T_Coords const& GetPoints() const { return m_LocalCoords; }
	uint32_t GetWidth() const { return m_Width; }
	uint32_t GetHeight() const { return m_Height; }
	char GetSymbol(T_Coord const& /*coord*/) const { return m_Symbol; }

private:
	T_Coords m_LocalCoords = {};
	uint32_t m_Width = 0;
	uint32_t m_Height = 0;
	char m_Symbol = ' ';
};
