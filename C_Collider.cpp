#include "C_Collider.hpp"
#include "C_GameObject.hpp"

void C_Collider::Update()
{
	assert(GetGameObject() != nullptr && "Collider has no game object assigned!");

	m_GlobalCoords = GetGameObject()->GetGlobalCoords();
	
	T_CoordElement xMin = std::numeric_limits<T_CoordElement>::max();
	T_CoordElement xMax = std::numeric_limits<T_CoordElement>::lowest();
	T_CoordElement yMin = std::numeric_limits<T_CoordElement>::max();
	T_CoordElement yMax = std::numeric_limits<T_CoordElement>::lowest();
	for (T_Coord const& coord : m_GlobalCoords)
	{
		xMin = std::min(xMin, coord.x);
		xMax = std::max(xMax, coord.x);
		yMin = std::min(yMin, coord.y);
		yMax = std::max(yMax, coord.y);
	}

	m_Bounds = { { xMin, yMin }, { xMax, yMax } };
}

bool C_Collider::CheckCollision(C_Collider& other, T_Pred setInfoPredicate)
{
	bool doesCollide = DoesCollideWith(other);
	
	if (setInfoPredicate(*this))
	{
		assert(other.GetGameObject() != nullptr && "Collider has no game object assigned!");
		m_CollisionInfo = { other.GetGameObject()->GetType(), doesCollide };
	}

	if (setInfoPredicate(other))
	{
		assert(this->GetGameObject() != nullptr && "Collider has no game object assigned!");
		other.m_CollisionInfo = { this->GetGameObject()->GetType(), doesCollide };
	}

	return doesCollide;
}
