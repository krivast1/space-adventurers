#pragma once

#include <vector>
#include <limits>

#include "S_Vector2D.hpp"

enum class E_Orientation : uint32_t
{
    NONE  = 0,
    UP    = 1,
    RIGHT = 2,
    DOWN  = 3,
    LEFT  = 4
};

template<typename T>
struct S_Orientation
{
    S_Orientation(E_Orientation orientation) : m_Orientation(orientation) {}

    S_Vector2D<T> const& AsVector() const { return s_Values[GetEnumUnderlyingType(m_Orientation)]; }
    E_Orientation const& AsEnum() const { return m_Orientation; }

    void SetOrientation(E_Orientation orientation) { m_Orientation = orientation; }

private:
    E_Orientation m_Orientation = E_Orientation::NONE;

    static const std::vector<S_Vector2D<T>> s_Values;
};

template<typename T> const std::vector<S_Vector2D<T>> S_Orientation<T>::s_Values =
    { S_Vector2D<T>::ZERO, S_Vector2D<T>::UP, S_Vector2D<T>::RIGHT, S_Vector2D<T>::DOWN, S_Vector2D<T>::LEFT };


class C_Transform
{
public:

    C_Transform() = default;
    
    C_Transform(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
        : m_Position(position), m_Direction(direction.Normalized()), m_Speed(speed) {}

    C_Transform(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed)
        : m_Position(std::move(position)), m_Direction(std::move(direction.Normalize())), m_Speed(speed) {}

    DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_Transform)

    virtual T_Coords const& GetLocalCoords() const = 0;

    void SetPosition(S_Vector2D<float> const& position);
    void SetPosition(S_Vector2D<float>&& position);

    void SetDirection(S_Vector2D<float> const& direction) { m_Direction = direction.Normalized(); }
    void SetDirection(S_Vector2D<float>&& direction) { m_Direction = std::move(direction.Normalize()); }

    void SetSpeed(float speed) { m_Speed = speed; }

    void SetPositionBounds(S_Bounds<float> const& bounds) { m_PositionBounds = bounds; }
    void SetPositionBounds(S_Bounds<float>&& bounds) { m_PositionBounds = std::move(bounds); }

    T_Coords GetGlobalCoords() const;
    T_Coord LocalToGlobal(T_Coord const& localCoord) const { return GetPosition().CastTo<T_CoordElement>() += localCoord; }
    S_Vector2D<float> const& GetPosition() const { return m_Position; }
    S_Vector2D<float> const& GetDirection() const { return m_Direction; }
    float GetSpeed() const { return m_Speed; }

    void Move();

private:
    S_Vector2D<float> m_Position = {};
    S_Vector2D<float> m_Direction = {};
    float m_Speed = 0;

    S_Bounds<float> m_PositionBounds = {
        { std::numeric_limits<float>::lowest(), std::numeric_limits<float>::lowest()},
        { std::numeric_limits<float>::max(), std::numeric_limits<float>::max() }
    };
};
