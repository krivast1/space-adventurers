#pragma once

#include "C_Shot.hpp"

#define SHOT_SPEED 2.0f

class C_Ship : public C_GameObject
{
public:
    C_Ship(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
        : C_GameObject(position, direction, speed)
        , m_ShotSpeed(std::max(speed + 0.5f, SHOT_SPEED))
    {}

    C_Ship(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed)
        : C_GameObject(std::move(position), std::move(direction), speed)
        , m_ShotSpeed(std::max(speed + 0.5f, SHOT_SPEED))
    {}

    DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_Ship)

    virtual void Draw(C_Canvas& canvas) const override;
    virtual T_Coords const& GetLocalCoords() const override;
    virtual E_GameObjectType GetType() const override { return E_GameObjectType::SHIP; }

    virtual std::shared_ptr<C_Shot> Shoot() const;

    void SetOrientation(E_Orientation orientation);
    S_Orientation<float> const& GetOrientation() const { return m_Orientation; }

private:
    using C_Transform::SetDirection;

    virtual void UpdateInternal() override;
    virtual void HandleCollisionInternal(S_CollisionInfo const& collisionInfo) override;

protected:
    float const m_ShotSpeed;
    T_Coords const m_LocalCoords = { {0, 0} };

    S_Orientation<float> m_Orientation{ E_Orientation::UP };
};
