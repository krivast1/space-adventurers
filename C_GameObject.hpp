#pragma once

#include <memory>

#include "C_EmptyCollider.hpp"
#include "C_Transform.hpp"
#include "C_Canvas.hpp"

enum class E_GameObjectType : uint32_t
{
    OTHER = 0,
    SHIP = 1,
    SHOT = 2,
    ASTEROID = 3,
    BOMB = 4,

    COUNT
};

class C_GameObject : public C_Transform
{
public:
    using T_ColliderPtr = std::unique_ptr<C_Collider>;

    C_GameObject() = default;
    
    C_GameObject(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
        : C_Transform(position, direction, speed){}

    C_GameObject(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed)
        : C_Transform(std::move(position), std::move(direction), speed) {}

    DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_GameObject)

    virtual void Draw(C_Canvas& canvas) const = 0;
    virtual E_GameObjectType GetType() const = 0;

    virtual bool IsPlayer() const { return false; }

    void Update();
    void SetCollider(T_ColliderPtr ptr);
    void HandleCollision(S_CollisionInfo const& collisionInfo);

    C_Collider const& GetCollider() const { return *m_ColliderPtr; }
    C_Collider& GetCollider() { return *m_ColliderPtr; }

private:
    virtual void UpdateInternal() = 0;
    virtual void HandleCollisionInternal(S_CollisionInfo const& collisionInfo) = 0;

    T_ColliderPtr m_ColliderPtr = std::make_unique<C_EmptyCollider>();
};

