#pragma once

#include "C_GameObject.hpp"

class C_Shot : public C_GameObject
{
public:
    C_Shot() = delete;

    C_Shot(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
        : C_GameObject(position, direction, speed) {}

    C_Shot(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed)
        : C_GameObject(std::move(position), std::move(direction), speed) {}

    DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_Shot)

    virtual void Draw(C_Canvas& canvas) const override;
    virtual T_Coords const& GetLocalCoords() const override;
    virtual E_GameObjectType GetType() const override { return E_GameObjectType::SHOT; }

    virtual bool IsFromPlayer() const { return false; }

private:
    virtual void UpdateInternal() override;
    virtual void HandleCollisionInternal(S_CollisionInfo const& collisionInfo) override;

    T_Coords const m_LocalCoords = { {0, 0} };
};
