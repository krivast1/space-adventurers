#pragma once

#include "C_GameObject.hpp"
#include "C_SpriteView.hpp"

#define BOMB_MIN_SPEED 0.2f
#define BOMB_MAX_SPEED 0.8f

class C_Bomb : public C_GameObject
{
    using T_GameObjectPtr = std::shared_ptr<C_GameObject>;

public:

    C_Bomb() = delete;

    C_Bomb(S_Vector2D<float> const& position,
           S_Vector2D<float> const& direction,
           float speed,
           T_GameObjectPtr const& target)
        : C_GameObject(position, direction, speed), m_Target(target)
    {}

    C_Bomb(S_Vector2D<float>&& position,
           S_Vector2D<float>&& direction,
           float speed,
           T_GameObjectPtr const& target)
        : C_GameObject(std::move(position), std::move(direction), speed), m_Target(target)
    {}

    DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_Bomb)

    virtual void Draw(C_Canvas& canvas) const override;
    virtual T_Coords const& GetLocalCoords() const override;
    virtual E_GameObjectType GetType() const override { return E_GameObjectType::BOMB; }

private:
    virtual void UpdateInternal() override;
    virtual void HandleCollisionInternal(S_CollisionInfo const& collisionInfo) override;

    T_Coords const m_LocalCoords = { {0, 0} };
    T_GameObjectPtr m_Target = nullptr;
};
