#include "C_Asteroid.hpp"

#include <array>

static C_SpriteView::T_Sprite const s_Sprite1 = {
    { 0, 1, 1, 0, 0 },
    { 0, 1, 1, 1, 0 },
    { 1, 1, 1, 1, 1 },
    { 0, 1, 1, 1, 0 },
    { 0, 0, 1, 1, 0 },
};

static C_SpriteView::T_Sprite const s_Sprite2 = {
    { 0, 1, 1, 1, 0 },
    { 1, 1, 1, 1, 1 },
    { 0, 1, 1, 1, 0 },
};

static C_SpriteView::T_Sprite const s_Sprite3 = {
    { 0, 1, 1, 0 },
    { 0, 1, 1, 1 },
    { 1, 1, 1, 1 },
    { 0, 1, 1, 1 },
};

static C_SpriteView::T_Sprite const s_Sprite4 = {
    { 1, 1, 0 },
    { 1, 1, 1 },
    { 0, 1, 1 },
};

static std::array<C_SpriteView::T_Sprite, 4> const s_Sprites = { s_Sprite1, s_Sprite2, s_Sprite3, s_Sprite4 };

C_Asteroid::C_Asteroid(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
    : C_GameObject(position, direction, speed)
    , spriteView(s_Sprites[rand() % s_Sprites.size()], '@')
{
}

C_Asteroid::C_Asteroid(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed)
    : C_GameObject(std::move(position), std::move(direction), speed)
    , spriteView(s_Sprites[rand() % s_Sprites.size()], '@')
{
}

void C_Asteroid::Draw(C_Canvas& canvas) const
{
    for (T_Coord const& coord : spriteView.GetPoints())
    {
        canvas.SetPixel(LocalToGlobal(coord), spriteView.GetSymbol(coord));
    }
}

T_Coords const& C_Asteroid::GetLocalCoords() const
{
    return spriteView.GetPoints();
}

void C_Asteroid::UpdateInternal()
{
    Move();
}

void C_Asteroid::HandleCollisionInternal(S_CollisionInfo const& /*collisionInfo*/)
{
}
