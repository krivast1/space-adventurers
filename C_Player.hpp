#pragma once

#include "C_Ship.hpp"

class C_Player : public C_Ship
{
    class C_PlayerShot : public C_Shot
    {
    public:
        C_PlayerShot(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
            : C_Shot(position, direction, speed) {}

        C_PlayerShot(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed)
            : C_Shot(std::move(position), std::move(direction), speed) {}

        DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_PlayerShot)

        virtual bool IsFromPlayer() const final override { return true; }
    };

public:
    C_Player(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed)
        : C_Ship(position, direction, speed) {}

    C_Player(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed)
        : C_Ship(std::move(position), std::move(direction), speed) {}

    DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_Player)

    virtual std::shared_ptr<C_Shot> Shoot() const final override;

	virtual bool IsPlayer() const final override { return true; }
};
