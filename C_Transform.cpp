#include "C_Transform.hpp"
#include "C_Transform.hpp"
#include "C_Transform.hpp"
#include "C_Transform.hpp"

void C_Transform::SetPosition(S_Vector2D<float> const& position)
{
    m_Position = position;
    m_Position.ToBounds(m_PositionBounds);
}

void C_Transform::SetPosition(S_Vector2D<float>&& position)
{
    m_Position = std::move(position);
    m_Position.ToBounds(m_PositionBounds);
}

T_Coords C_Transform::GetGlobalCoords() const
{
    T_Coords coords = GetLocalCoords();
    
    for (T_Coord& coord : coords)
    {
        coord = LocalToGlobal(coord);
    }

    return coords;
}

void C_Transform::Move()
{
    // x speed is scaled to visually match the y speed because of the character size and spacing
    m_Position += { m_Direction.x * m_Speed * 2.0f, m_Direction.y * m_Speed };
    m_Position.ToBounds(m_PositionBounds);
}
