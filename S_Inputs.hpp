#pragma once

#include "common.hpp"
#include "S_Vector2D.hpp"
#include "C_Transform.hpp"

struct S_Inputs
{
    E_Orientation orientation = E_Orientation::NONE;
    bool fire = false;
};
