#pragma once

#include <cstdint>
#include <cassert>
#include <type_traits>

#define DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(className)     \
        className& operator=(className const&) = default; \
        className& operator=(className&&) = default;      \
        className(className const&) = default;            \
        className(className&&) = default;                 \
        virtual ~className() = default;                   \


template<typename TEnum>
inline constexpr auto GetEnumUnderlyingType(TEnum e) -> typename std::underlying_type<TEnum>::type 
{
   return static_cast<typename std::underlying_type<TEnum>::type>(e);
}

inline bool DidPassRandom() { return (rand() & 1) == 0; }

inline float GetRandomFloat(float a, float b)
{
    return a + ((float)rand() / (float)(RAND_MAX)) * std::abs(b - a);
}
