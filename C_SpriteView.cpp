#include "C_SpriteView.hpp"

C_SpriteView::C_SpriteView(T_Sprite const& shape, char symbol)
	: m_Height(static_cast<uint32_t>(shape.size())), m_Symbol(symbol)
{
	uint32_t width = 0;

	for (uint32_t y = 0; y < shape.size(); ++y)
	{
		uint32_t rowSize = static_cast<uint32_t>(shape[y].size());
		width = rowSize > width ? rowSize : width;

		for (uint32_t x = 0; x < rowSize; ++x)
		{
			if (shape[y][x] == true)
			{
				m_LocalCoords.push_back({
					static_cast<T_CoordElement>(x),
					static_cast<T_CoordElement>(y)
				});
			}
		}
	}
	m_Width = width;

	// Centralize points, so the origin is not in the top left corner, but in the middle
	T_Coord offset = { static_cast<T_CoordElement>(m_Width / 2), static_cast<T_CoordElement>(m_Height / 2) };
	for (T_Coord& coord : m_LocalCoords)
	{
		coord -= offset;
	}
}
