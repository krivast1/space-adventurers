#pragma once

#include "C_GameObject.hpp"
#include "C_SpriteView.hpp"

#define ASTEROID_MIN_SPEED 0.1f
#define ASTEROID_MAX_SPEED 1.2f

class C_Asteroid : public C_GameObject
{
public:
    C_Asteroid(S_Vector2D<float> const& position, S_Vector2D<float> const& direction, float speed);
    C_Asteroid(S_Vector2D<float>&& position, S_Vector2D<float>&& direction, float speed);

    DECLARE_DEFAULT_VIRTUAL_DESTRUCTOR(C_Asteroid)

    virtual void Draw(C_Canvas& canvas) const override;
    virtual T_Coords const& GetLocalCoords() const override;
    virtual E_GameObjectType GetType() const override { return E_GameObjectType::ASTEROID; }

private:
    virtual void UpdateInternal() override;
    virtual void HandleCollisionInternal(S_CollisionInfo const& collisionInfo) override;

    C_SpriteView const spriteView;
};
