#include "C_Bomb.hpp"

void C_Bomb::Draw(C_Canvas& canvas) const
{
    canvas.SetPixel(LocalToGlobal(m_LocalCoords[0]), '*');
}

T_Coords const& C_Bomb::GetLocalCoords() const
{
    return m_LocalCoords;
}

void C_Bomb::UpdateInternal()
{
    if (m_Target != nullptr)
    {
        SetDirection(m_Target->GetPosition() - GetPosition());
    }
    
    Move();
}

void C_Bomb::HandleCollisionInternal(S_CollisionInfo const& /*collisionInfo*/)
{
}
