#include "C_GameObject.hpp"

void C_GameObject::Update()
{
    UpdateInternal();
    if (m_ColliderPtr != nullptr)
    {
        m_ColliderPtr->Update();
    }
}

void C_GameObject::SetCollider(T_ColliderPtr ptr)
{
    if (ptr != nullptr)
    {
        m_ColliderPtr = std::move(ptr);
    }
    else
    {
        m_ColliderPtr = std::make_unique<C_EmptyCollider>();
    }
}

void C_GameObject::HandleCollision(S_CollisionInfo const& collisionInfo)
{
    HandleCollisionInternal(collisionInfo);
}
