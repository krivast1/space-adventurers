#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#endif // _WIN32

#include <sstream>

#include "C_Window.hpp"

#ifdef _WIN32
static HANDLE outHandle = GetStdHandle(STD_OUTPUT_HANDLE);
static CONSOLE_SCREEN_BUFFER_INFO startInfo;
#endif // _WIN32

C_Window::C_Window(std::ostream& outputStream, uint32_t width, uint32_t height) : m_OutputStream(outputStream), m_Canvas(width, height)
{
#ifdef _WIN32
    GetConsoleScreenBufferInfo(outHandle, &startInfo);
    SetConsoleMode(outHandle, ENABLE_INSERT_MODE);
    SetConsoleDisplayMode(outHandle, CONSOLE_FULLSCREEN_MODE, 0);
#endif // _WIN32
}

void C_Window::Render() const
{    
    std::stringstream buffer;

#ifndef _WIN32
    buffer << ANSI_CLEAR << ANSI_COLOR_RESET;
#endif // _WIN32

    std::string score = "Score: " + std::to_string(m_Score);

#ifdef _WIN32
    CONSOLE_SCREEN_BUFFER_INFO info;
    GetConsoleScreenBufferInfo(outHandle, &info);
    uint32_t windowSizeLeft = info.srWindow.Right - info.srWindow.Left - score.size();
    for (uint32_t i = 0; i < windowSizeLeft; ++i)
    {
        score += ' ';
    }
#endif // _WIN32

    buffer << score << std::endl;

    buffer << '\r';
    for (uint32_t w = 0; w < m_Canvas.GetWidth(); ++w)
    {
        buffer << '-';
    }
    buffer << std::endl;

    auto it = m_Canvas.GetBuffer().begin();
    for (uint32_t h = 0; h < m_Canvas.GetHeight(); ++h)
    {
        buffer << '\r';
        for (uint32_t w = 0; w < m_Canvas.GetWidth(); ++w)
        {
            buffer << *it++;
        }
        buffer << std::endl;
    }

    buffer << '\r' << m_Text << std::endl;

#ifdef _WIN32
    std::string const str = buffer.str();
    SetConsoleCursorPosition(outHandle, startInfo.dwCursorPosition);
    WriteConsoleA(outHandle, str.c_str(), str.size(), NULL, NULL);
#else
    m_OutputStream << buffer.str();
    m_OutputStream.flush();
#endif // _WIN32
}
