#include <iostream>

#include "C_Ship.hpp"
#include "C_PixelCollider.hpp"

void C_Ship::Draw(C_Canvas& canvas) const
{
    char c;
    switch (m_Orientation.AsEnum())
    {
        case E_Orientation::UP:
            c = '^';
            break;
        case E_Orientation::RIGHT:
            c = '>';
            break;
        case E_Orientation::DOWN:
            c = 'v';
            break;  
        case E_Orientation::LEFT:
            c = '<';
            break;
        default:
            c = 'E';
            std::cerr << "Error: Unhandled C_Ship orientation!" << std::endl;
            break;
    }

    canvas.SetPixel(LocalToGlobal(m_LocalCoords[0]), c);
}

T_Coords const& C_Ship::GetLocalCoords() const
{
    return m_LocalCoords;
}

std::shared_ptr<C_Shot> C_Ship::Shoot() const
{
    S_Vector2D<float> direction = m_Orientation.AsVector();
    S_Vector2D<float> position = GetPosition() + direction;
    
    std::shared_ptr<C_Shot> shot = std::make_shared<C_Shot>(position, direction, m_ShotSpeed);
    shot->SetCollider(std::make_unique<C_PixelCollider>(shot));

    return shot;
}

void C_Ship::SetOrientation(E_Orientation orientation)
{
    if (orientation != E_Orientation::NONE)
    {
        m_Orientation.SetOrientation(orientation);
    }

    SetDirection(m_Orientation.AsVector());
}

void C_Ship::UpdateInternal()
{
    Move();
}

void C_Ship::HandleCollisionInternal(S_CollisionInfo const& /*collisionInfo*/)
{

}
