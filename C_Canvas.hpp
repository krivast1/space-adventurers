#pragma once

#include <string>

#include "common.hpp"
#include "S_Vector2D.hpp"

class C_Canvas
{
public:
    C_Canvas(uint32_t width, uint32_t height);

    template<typename T>
    void SetPixel(S_Vector2D<T> const& position, char symbol);
    
    void Clear();

    uint32_t GetWidth() const { return m_Width; }
    uint32_t GetHeight() const { return m_Height; }
    std::string const& GetBuffer() const { return m_Buffer; }

private:
    uint32_t m_Width;
    uint32_t m_Height;
    std::string m_Buffer;
};

template<typename T>
void C_Canvas::SetPixel(S_Vector2D<T> const& position, char symbol)
{
    uint32_t x = static_cast<uint32_t>(position.x);
    uint32_t y = static_cast<uint32_t>(position.y);

    if (x < m_Width && y < m_Height)
    {
        m_Buffer[y * m_Width + x] = symbol;
    }
}
