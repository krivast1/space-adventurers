#pragma once

#include <ostream>

#include "C_Canvas.hpp"

#define ANSI_CLEAR "\x1B[2J\x1B[H"
#define ANSI_COLOR_RESET "\x1B[m"

class C_Window
{
public:
    C_Window(std::ostream& outputStream, uint32_t width, uint32_t height);

    void AddScore(uint64_t inc) { m_Score += inc; }
    void ResetScore() { m_Score = 0; }

    void SetText(std::string const& text) { m_Text = text; }
    void ClearText() { m_Text = "                                            "; } // tmp solution

    C_Canvas& GetCanvas() { return m_Canvas; }
    C_Canvas const& GetCanvas() const { return m_Canvas; }

    void Render() const;

private:
    std::ostream& m_OutputStream;
    C_Canvas m_Canvas;
    std::string m_Text;
    uint64_t m_Score = 0;
};
