#include "C_Canvas.hpp"

C_Canvas::C_Canvas(uint32_t width, uint32_t height) : m_Width(width), m_Height(height), m_Buffer(std::string(width * height, ' '))
{
}

void C_Canvas::Clear()
{
    for (char& c : m_Buffer)
    {
        c = ' ';
    }
}
